/*
    This file is part of ucmf - microcontroller make frontend
    Copyright (C) 2021-2024  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef UCMF__HARDWARECONFIG_H
#define UCMF__HARDWARECONFIG_H


#include <string>
#include <vector>


namespace UCMF
{
    /**
     * The HardwareConfig class represents the fundamental configuration
     * of the hardware that is necessary to give to compilers so that
     * programs can be written hardware-independent.
     */
    class HardwareConfig
    {
        public:

        /**
         * Whether the microcontroller uses little-endian encoding (true)
         * or big-endian encoding (false). Defaults to true.
         */
        bool is_little_endian = true;

        /**
         * Generates a list of C/C++ definitions for the hardware configuration.
         */
        std::vector<std::string> getDefinitions()
        {
            std::vector<std::string> definitions;
            if (this->is_little_endian) {
                definitions.push_back("UCMF_UC_LITTLE_ENDIAN");
            } else {
                definitions.push_back("UCMF_UC_BIG_ENDIAN");
            }
            return definitions;
        }
    };
}

#endif
