/*
    This file is part of ucmf - microcontroller make frontend
    Copyright (C) 2021-2024  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Generator.h"


using namespace UCMF;


void Generator::setConfig(std::shared_ptr<Config> config)
{
    this->config = config;
}


void Generator::addSourceFile(const std::string& source_file)
{
    if (this->all_sources_in_c) {
        size_t extension_start = source_file.rfind('.');
        if (extension_start != std::string::npos) {
            auto extension = source_file.substr(extension_start + 1, std::string::npos);
            if ((extension == "cpp") || (extension == "cc") || (extension == "ino")) {
                this->all_sources_in_c = false;
            }
        }
    }
    this->source_files.push_back(source_file);
}


std::string Generator::buildMakefile()
{
    if (this->config == nullptr) {
        return "";
    }

    std::string output;

    auto uc_family = this->config->getEntry("UC_FAMILY");
    auto uc_package = this->config->getEntry("UC_PACKAGE");
    auto model = this->config->getEntry("UC_MODEL");

    std::string model_uppercase = model;
    std::transform(model.begin(), model.end(), model_uppercase.begin(), ::toupper);

    //Set the C/C++ language version, if not explicitly set
    //in the configuration:
    std::string lang_version = this->config->getEntry("LANG_VERSION");
    if (lang_version.empty()) {
        if (this->all_sources_in_c) {
            lang_version = "C99";
        } else {
            lang_version = "c++17";
        }
        this->config->setEntry("LANG_VERSION", lang_version);
    }

    //Generate the compiler definitions:

    std::vector<std::string> compiler_definitions;

    compiler_definitions.push_back(
        fmt::format(
            "UC_MODEL_{}",
            model_uppercase
            )
        );
    if (!uc_package.empty()) {
        std::transform(uc_package.begin(), uc_package.end(), uc_package.begin(), ::toupper);
        compiler_definitions.push_back(
            fmt::format(
                "UC_PACKAGE_{}",
                uc_package
                )
            );
    }
    //Add definitions for the hardware configuration:
    auto hardware_definitions = this->hardware_config.getDefinitions();
    for (auto definition: hardware_definitions) {
        compiler_definitions.push_back(definition);
    }

    std::string output_file = "output.bin";
    std::string all_source_targets;

    for (auto source_file: this->source_files) {
        std::string source_target = this->getTargetFileNameForSource(source_file);
        source_target = std::filesystem::path(source_target).filename();
        if (!all_source_targets.empty()) {
            all_source_targets += " ";
        }
        all_source_targets += source_target;

        //Generate the code for the source file target:
        output += this->buildMakefileTargetForSourceFile(
            source_file,
            source_target,
            compiler_definitions
            ) + "\n\n";
    }

    //Generate the code for the build target:
    output += this->buildMakefileTargetForBinary(
        all_source_targets,
        output_file
        ) + "\n\n";

    //Generate the code for the upload target:

    output += this->buildMakefileTargetForUploading(
        output_file
        ) + "\n\n";

    //Generate the clean target:
    output += this->buildMakefileTargetForCleanup(
        all_source_targets,
        output_file
        ) + "\n\n";

    return output;
}


std::string Generator::getTargetFileNameForSource(
    const std::string& source_file
    )
{
    return source_file + ".o";
}
