/*
    This file is part of ucmf - microcontroller make frontend
    Copyright (C) 2021-2024  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "GameboyGenerator.h"


using namespace UCMF;


std::string GameboyGenerator::getTargetFileNameForSource(
    const std::string& source_file
    )
{
    return source_file + ".rel";
}


std::string GameboyGenerator::buildMakefileTargetForSourceFile(
    const std::string& source_file,
    const std::string& source_target,
    const std::vector<std::string> compiler_definitions
    )
{
    std::string model           = this->config->getEntry("UC_MODEL");
    std::string model_uppercase = model;
    std::transform(model.begin(), model.end(), model_uppercase.begin(), ::toupper);
    //Generate the compiler parameters:
    std::string compiler_parameters = "-D UC_FAMILY_GAMEBOY";
    compiler_parameters += fmt::format(" -D UC_MODEL_{}", model_uppercase);

    //Add definitions for the hardware configuration:
    auto hardware_definitions = this->hardware_config.getDefinitions();
    for (auto definition: hardware_definitions) {
        compiler_parameters += " -D " + definition;
    }
    //Add the remaining parameters:
    compiler_parameters += fmt::format(
        " -m{} -c -o {} {}",
        model,
        source_target,
        source_file
        );

    return fmt::format(
        "{}: {}\n\tsdcc {}\n\n",
        source_target,
        source_file,
        compiler_parameters
        );
}


std::string GameboyGenerator::buildMakefileTargetForBinary(
    const std::string& all_source_targets,
    const std::string& binary_file_name
    )
{
    std::string program_name = this->config->getEntry("PROGRAM_NAME", "GAMEBOY");
    if (program_name.empty()) {
        program_name = this->config->getEntry("PROJECT_NAME");
    }
    if (program_name.length() > 8) {
        //Shorten the program name to 8 characters:
        program_name = program_name.substr(0, 8);
    }

    std::string output = fmt::format(
        "build: {}\n\tsdcc -m{} -o {} {}\n",
        all_source_targets,
        this->config->getEntry("UC_MODEL"),
        binary_file_name,
        all_source_targets
        );
    output += fmt::format(
        "\tmakebin -yn \"{}\", -Z {} > output.gb\n\n",
        program_name,
        binary_file_name
        );
    return output;
}


std::string GameboyGenerator::buildMakefileTargetForUploading(
    const std::string& binary_file_name
    )
{
    //This generator has no upload target, but it must be generated
    //to comply with the UCMF guidelines:
    return "upload:\n\t@echo \"The upload target is not supported for the Gameboy platform!\"\n\n";
}


std::string GameboyGenerator::buildMakefileTargetForCleanup(
    const std::string& all_source_targets,
    const std::string& binary_file_name
    )
{
    return fmt::format(
        "clean:\n\trm {} output.gb {}\n\n",
        binary_file_name,
        all_source_targets
        );
}
