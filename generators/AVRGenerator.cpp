/*
    This file is part of ucmf - microcontroller make frontend
    Copyright (C) 2021-2023  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "AVRGenerator.h"
#include "../Config.h"


using namespace UCMF;


void AVRGenerator::setConfig(std::shared_ptr<Config> config)
{
    this->config = config;
    if (config->getEntry("UC_FAMILY") == "ARDUINO"
        && config->getEntry("UC_PACKAGE").empty()
        ) {
        //Set the package for the Arduino model since it isn't explicitly set:
        auto model = config->getEntry("UC_MODEL");
        auto avr_model = this->arduinoModelToAvrModel(model);
        if (avr_model.empty()) {
            throw std::invalid_argument(
                fmt::format(
                    "The arduino model \"{}\" is not supported!",
                    avr_model
                    )
                );
        }
        this->config->setEntry("UC_PACKAGE", "ARDUINO_" + model);
        this->config->setEntry("MODEL", model, "ARDUINO");
        this->config->setEntry("UC_MODEL", avr_model);
    }
}



std::string AVRGenerator::modelToAvrdudePart(const std::string& model)
{
    if ((model.find("attiny") != std::string::npos) && model.length() > 6) {
        std::string model_number = model.substr(6, std::string::npos);
        return "t" + model_number;
    } else if ((model.find("atmega") != std::string::npos) && model.length() > 6) {
        std::string model_number = model.substr(6, std::string::npos);
        return "m" + model_number;
    }
    //Unknown avrdude part.
    return "";
}


std::string AVRGenerator::arduinoModelToAvrModel(const std::string& arduino_model)
{
    if (arduino_model == "uno") {
        return "atmega328p";
    } else if (arduino_model == "micro") {
        return "atmega32u4";
    }
    //Unknown or unsupported arduino model.
    return "";
}


std::string AVRGenerator::buildMakefileTargetForSourceFile(
    const std::string& source_file,
    const std::string& source_target,
    const std::vector<std::string> compiler_definitions
    )
{
    std::string compiler_parameters = "-Os -D UC_FAMILY_AVR";
    for (auto definition: compiler_definitions) {
        compiler_parameters += " -D " + definition;
    }

    //Check the file extension:
    size_t extension_start = source_file.rfind('.');
    if (extension_start != std::string::npos) {
        auto extension = source_file.substr(extension_start + 1, std::string::npos);
        if (extension == "ino") {
            //Arduino sketch files must be explicitly declared as C++ files:
            compiler_parameters += " -x c++";
            //Furthermore, the include path for Arduino.h must be set:
            auto arduino_header_location = this->config->getEntry("HEADER_LOCATION", "ARDUINO");
            if (arduino_header_location.empty()) {
                throw std::invalid_argument(
                    fmt::format(
                        "ARDUINO::HEADER_LOCATION is not set in ucmf.config or ucmf-local.config! "
                        "The Arduino sketch \"{}\" cannot be compiled without it!",
                        source_file
                        )
                    );
            } else {
                //Set the include path for the Arduino.h header:
                compiler_parameters += fmt::format(" -I {}", arduino_header_location);
            }
        }
    }

    compiler_parameters += fmt::format(
        " -mmcu={} --std={} -c -o {} {}",
        this->config->getEntry("UC_MODEL"),
        this->config->getEntry("LANG_VERSION"),
        source_target,
        source_file
        );

    if (this->all_sources_in_c) {
        //Build the target for a C source file.
        return fmt::format(
            "{}: {}\n\tavr-gcc {}",
            source_target,
            source_file,
            compiler_parameters
            );
    } else {
        //Build the target for a C++ source file.
        return fmt::format(
            "{}: {}\n\tavr-g++ {}",
            source_target,
            source_file,
            compiler_parameters
            );
    }
}


std::string AVRGenerator::buildMakefileTargetForBinary(
    const std::string& all_source_targets,
    const std::string& binary_file_name
    )
{
    if (this->all_sources_in_c) {
        return fmt::format(
            "build: {}\n\tavr-gcc -Os -mmcu={} --std={} -o {} {}",
            all_source_targets,
            this->config->getEntry("UC_MODEL"),
            this->config->getEntry("LANG_VERSION"),
            binary_file_name,
            all_source_targets
            );
    } else {
        return fmt::format(
            "build: {}\n\tavr-g++ -Os -mmcu={} --std={} -o {} {}",
            all_source_targets,
            this->config->getEntry("UC_MODEL"),
            this->config->getEntry("LANG_VERSION"),
            binary_file_name,
            all_source_targets
            );
    }
}


std::string AVRGenerator::buildMakefileTargetForUploading(
    const std::string& binary_file_name
    )
{
    std::string output;
    if (this->config->getEntry("UC_FAMILY") == "ARDUINO") {
        //For Arduino microcontrollers, the .eeprom segment from the ELF
        //executable (that is the output file) needs to be stripped
        //since it is the only part that is being uploaded.
        //Therefore, an intermediate target is being generated.
        output += fmt::format(
            "{}.ihex: build\n", binary_file_name
            );
        output += fmt::format(
            "\tavr-objcopy -O ihex -R .eeprom {0} {0}.ihex\n\n",
            binary_file_name
            );

        output += fmt::format("upload: {}.ihex\n", binary_file_name);
    } else {
         output += fmt::format("\nupload: {}\n", binary_file_name);
    }

    //Generate the code for the upload target by generating
    //the avrdude commands:
    auto avrdude_part_name = AVRGenerator::modelToAvrdudePart(this->config->getEntry("UC_MODEL"));
    auto programmer        = this->config->getEntry("PROGRAMMER", "AVR");
    auto port              = this->config->getEntry("PORT", "AVR");
    auto baudrate          = this->config->getEntry("BAUDRATE", "AVR");

    std::string avrdude_command_prefix = "avrdude ";
    bool missing_config = false;

    if (avrdude_part_name.empty()) {
        missing_config = true;
        avrdude_command_prefix += "-p PART ";
    } else {
        avrdude_command_prefix += fmt::format("-p {} ", avrdude_part_name);
    }
    if (programmer.empty()) {
        missing_config = true;
        avrdude_command_prefix += "-c PROGRAMMER ";
    } else {
        avrdude_command_prefix += fmt::format("-c {} ", programmer);
    }
    if (port.empty()) {
        missing_config = true;
        avrdude_command_prefix += "-P PORT ";
    } else {
        avrdude_command_prefix += fmt::format("-P {} ", port);
    }
    if (baudrate.empty()) {
        missing_config = true;
        avrdude_command_prefix += "-b BAUDRATE ";
    } else {
        avrdude_command_prefix += fmt::format("-b {} ", baudrate);
    }

    std::string avrdude_upload_command = avrdude_command_prefix;
    std::string avrdude_verify_command = avrdude_command_prefix;

    if(this->config->getEntry("UC_FAMILY") == "ARDUINO") {
        //Arduino
        avrdude_upload_command += fmt::format("-U flash:w:{}.ihex:i", binary_file_name);
        avrdude_verify_command += fmt::format("-U flash:v:{}.ihex:i", binary_file_name);
    } else {
        //Normal AVR
        avrdude_upload_command += fmt::format("-U flash:w:{}:r", binary_file_name);
        avrdude_verify_command += fmt::format("-U flash:v:{}:r", binary_file_name);
    }

    if (missing_config) {
        output += fmt::format(
            "#{}\n#{}\n\t@echo \"{}\"",
            avrdude_upload_command,
            avrdude_verify_command,
            "WARNING: Some parameters for avrdude could not be determined by ucmf! "
            "Please edit the makefile manually to set these in the upload target!"
            );
    } else {
        output += fmt::format(
            "\t{}\n\t{}",
            avrdude_upload_command,
            avrdude_verify_command
            );
    }

    return output;
}


std::string AVRGenerator::buildMakefileTargetForCleanup(
    const std::string& all_source_targets,
    const std::string& binary_file_name
    )
{
    if (this->config->getEntry("UC_FAMILY") == "ARDUINO") {
        return fmt::format(
            "clean:\n\trm {0} {0}.ihex {1}",
            binary_file_name,
            all_source_targets
            );
    } else {
        return fmt::format(
            "clean:\n\trm {} {}",
            binary_file_name,
            all_source_targets
            );
    }
}
