/*
    This file is part of ucmf - microcontroller make frontend
    Copyright (C) 2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef UCMF__AVRGENERATOR_H
#define UCMF__AVRGENERATOR_H


#include <algorithm>
#include <stdexcept>


#include "fmt/core.h"


#include "Generator.h"


namespace UCMF
{
    class AVRGenerator: public Generator
    {
        protected:

        /**
         * Converts an AVR microcontroller model name to the part name
         * avrdude uses for that model.
         *
         * @param const std::string& model The AVR microcontroller model name.
         *
         * @returns std::string The avrdude part name for the
         *    microcontroller model.
         */
        static std::string modelToAvrdudePart(const std::string& model);

        /**
         * Converts the arduino model name to the corresponding
         * AVR microcontroller name.
         *
         * @param const std::string& arduino_model The Arduino model name.
         *
         * @returns std::string The AVR model name for the arduino model name.
         */
        std::string arduinoModelToAvrModel(const std::string& arduino_model);

        public:

        /**
         * @see Generator::setConfig
         */
        virtual void setConfig(std::shared_ptr<Config> config) override;

        /**
         * @see Generator::buildMakefileTargetForSourceFile
         */
        virtual std::string buildMakefileTargetForSourceFile(
            const std::string& source_file,
            const std::string& source_target,
            const std::vector<std::string> compiler_definitions
            ) override;

        /**
         * @see Generator::buildMakefileTargetForBinary
         */
        virtual std::string buildMakefileTargetForBinary(
            const std::string& all_source_targets,
            const std::string& binary_file_name
            ) override;

        /**
         * @see Generator::buildMakefileTargetForUploading
         */
        virtual std::string buildMakefileTargetForUploading(
            const std::string& binary_file_name
            ) override;

        /**
         * @see Generator::buildMakefileTargetForCleanup
         */
        virtual std::string buildMakefileTargetForCleanup(
            const std::string& all_source_targets,
            const std::string& binary_file_name
            ) override;
    };
}


#endif
