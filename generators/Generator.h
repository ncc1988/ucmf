/*
    This file is part of ucmf - microcontroller make frontend
    Copyright (C) 2021-2024  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef UCMF__GENERATOR_H
#define UCMF__GENERATOR_H


#include <algorithm>
#include <filesystem>
#include <map>
#include <memory>
#include <string>
#include <vector>


#include "../Config.h"
#include "../HardwareConfig.h"


namespace UCMF
{
    /**
     * A Generator is a class that generates Makefile content for
     * a specific microcontroller family by using the correct compiler
     * and calling the correct tools for the compilation and the uploading
     * process.
     */
    class Generator
    {
        protected:

        /**
         * The (hard-coded) configuration for the microcontroller hardware.
         * This config is used to add certain C/C++ definitions when calling
         * the compiler so that microcontroller programs can be programmed
         * for different hardware.
         */
        HardwareConfig hardware_config;

        /**
         * The configuration for the microcontroller software project.
         */
        std::shared_ptr<Config> config;

        /**
         * The source files for the microcontroller software project.
         */
        std::vector<std::string> source_files;

        /**
         * A flag indicating whether all source code files are C source code
         * files (true) or not (false). Defaults to true.
         */
        bool all_sources_in_c = true;

        public:

        /**
         * Sets the configuration of the project.
         */
        virtual void setConfig(std::shared_ptr<Config> config);

        /**
         * Adds a source file for the makefile content to be generated.
         */
        virtual void addSourceFile(const std::string& source_file);

        /**
         * Generates the makefile code.
         */
        virtual std::string buildMakefile();

        /**
         * Generates the target file name from a source file name.
         *
         * By default, this just appends ".o" to the end of the source file
         * name. But certain compilers need a specific file extenstion to
         * detect compiled files. Therefore, generators may want to overwrite
         * this method to provide a custom target file name.
         *
         * @param const std::string& source_file The source file for which
         *     to generate a target file name.
         *
         * @returns std::string The target file name for the source file name.
         */
        virtual std::string getTargetFileNameForSource(
            const std::string& source_file
            );

        /**
         * Generates the code for a makefile target for one single source file.
         *
         * @param const std::string& source_file The source file for which to
         *    build a makefile target.
         *
         * @param const std::string& source_target The target for the
         *    source file. This is usually the file name of the compiled file.
         *
         * @param const std::vector<std::string> compiler_definitions
         *    The definitions the compiler shall make available to C/C++ code
         *    (doing the same thing as #define in a header file).
         *
         * @returns std::string The complete makefile code for the target.
         */
        virtual std::string buildMakefileTargetForSourceFile(
            const std::string& source_file,
            const std::string& source_target,
            const std::vector<std::string> compiler_definitions
            ) = 0;

        /**
         * Generates the code for the makefile target that builds the
         * binary out of all source code targets.
         *
         * @param const std::string& all_source_targets The list of all
         *     targets that are built out of source code files.
         *
         * @param const std::string& binary_file_name The file name of the
         *     binary that shall be built out of all source targets.
         *
         * @returns std::string The complete makefile code for the target.
         */
        virtual std::string buildMakefileTargetForBinary(
            const std::string& all_source_targets,
            const std::string& binary_file_name
            ) = 0;

        /**
         * Generates the code for the makefile target to upload the compiled
         * program to the microcontroller.
         *
         * @param const std::string& binary_file_name The file name of the
         *     binary file that shall be uploaded to the microcontroller.
         *
         * @returns std::string The complete makefile code for the target.
         */
        virtual std::string buildMakefileTargetForUploading(
            const std::string& binary_file_name
            ) = 0;

        /**
         * Generates the code for the makefile target to cleanup the build
         * directory from all files that were generated during compilation.
         *
         * @param const std::string& all_source_targets The list of all
         *     targets that are built out of source code files.
         *
         * @param const std::string& binary_file_name The file name of the
         *     binary file that shall be uploaded to the microcontroller.
         *
         * @returns std::string The complete makefile code for the target.
         */
        virtual std::string buildMakefileTargetForCleanup(
            const std::string& all_source_targets,
            const std::string& binary_file_name
            ) = 0;
    };
}


#endif
