/*
    This file is part of ucmf - microcontroller make frontend
    Copyright (C) 2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef UCMF__GAMEBOYGENERATOR_H
#define UCMF__GAMEBOYGENERATOR_H


#include <algorithm>


#include "Generator.h"


namespace UCMF
{
    class GameboyGenerator: public Generator
    {
        public:

        /**
         * @see Generator::getTargetFileNameForSource
         *
         * The sdcc compiler needs the target file name to have the extension
         * ".rel" to be able to identify compiled source files.
         */
        virtual std::string getTargetFileNameForSource(
            const std::string& source_file
            ) override;

        /**
         * @see Generator::buildMakefileTargetForSourceFile
         */
        virtual std::string buildMakefileTargetForSourceFile(
            const std::string& source_file,
            const std::string& source_target,
            const std::vector<std::string> compiler_definitions
            ) override;

        /**
         * @see Generator::buildMakefileTargetForBinary
         */
        virtual std::string buildMakefileTargetForBinary(
            const std::string& all_source_targets,
            const std::string& binary_file_name
            ) override;

        /**
         * @see Generator::buildMakefileTargetForUploading
         */
        virtual std::string buildMakefileTargetForUploading(
            const std::string& binary_file_name
            ) override;

        /**
         * @see Generator::buildMakefileTargetForCleanup
         */
        virtual std::string buildMakefileTargetForCleanup(
            const std::string& all_source_targets,
            const std::string& binary_file_name
            ) override;

    };
}


#endif
