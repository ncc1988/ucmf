/*
    This file is part of ucmf - microcontroller make frontend
    Copyright (C) 2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef UCMF__CONFIG_H
#define UCMF__CONFIG_H


#include <stdexcept>
#include <fstream>
#include <map>
#include <string>


#include "fmt/core.h"


namespace UCMF
{
    class Config
    {
        public:


        Config();


        /**
         * Reads an ucmf config file and stores its configuration
         * entries in the entries map.
         */
        void readConfigFile(const std::string& config_file_path);


        /**
         * Outputs all config entries in the format for config files.
         *
         * @returns std::string The config entries in the format for
         *     ucmf config files.
         */
        std::string toConfigFile();


        /**
         * Adds an entry to the configuration.
         */
        void addEntry(
            const std::string& key,
            const std::string& value,
            const std::string& section = ""
            );

        /**
         * setEntry is an alias to the addEntry method so that the counterpart
         * in the terminology for get/set is present.
         */
        void setEntry(
            const std::string& key,
            const std::string& value,
            const std::string& section = ""
            );


        /**
         * Checks whether the entry exists or not.
         *
         * @returns bool True, if the entry exists, false otherwise.
         */
        bool hasEntry(
            const std::string& key,
            const std::string& section = ""
            );


        /**
         * Returns the specified config entry or an empty string.
         *
         * @returns std::string The value of the config entry or an
         *     empty string if the entry doesn't exist.
         */
        std::string getEntry(
            const std::string& key,
            const std::string& section = ""
            );


        /**
         * Removes the specified config entry, if it exists.
         */
        void removeEntry(
            const std::string& key,
            const std::string& section = ""
            );


        protected:


        /**
         * This "two-dimensional" map uses the config section as the
         * first key and the component config key as the second key.
         *
         * The configuration in the global section has an empty string
         * as section.
         *
         * Examples:
         *
         * Setting an example entry in the global section:
         * this->entries[""]["project_name"] = "Test";
         *
         * Setting an entry in the avr section:
         * this->entries["avr"]["fuse_bits"] = "0xfe";
         */
        std::map<std::string, std::map<std::string, std::string>> entries;
    };
}


#endif
