/*
    This file is part of ucmf - microcontroller make frontend
    Copyright (C) 2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <iterator>
#include <memory>
#include <stdexcept>
#include <string>
#include <sstream>
#include <vector>


#include "fmt/core.h"


#include "Config.h"
#include "generators/AVRGenerator.h"
//#include "generators/GameboyGenerator.h"


const auto UCMF_VERSION = "0.2.0-dev";


void printHelp()
{
    std::cerr << "Usage: ucmf [DIRECTORY|--help|--license|--version]\n"
        "\tDIRECTORY\t\tThe directory where the project file is located.\n"
        "\t--help\t\t\tDisplays this help text.\n"
        "\t--help-uc-family\tDisplays all supported microcontroller families.\n"
        "\t--license\t\tDisplays license and copyright information.\n"
        "\t--version\t\tPrints the ucmf version and exits.\n"
              << std::endl;
}


void printUcFamilyList()
{
    std::cerr << "All supported microcontroller families for the UC_FAMILY configuration variable:\n\n"
        "AVR\tAtmel AVR microcontrollers. Used tools: avr-gcc, avrdude\n"
        "ARDUINO\tArduino microcontrollers with AVR microcontrollers. Used tools: avr-gcc, avrdude\n"
        //"GAMEBOY\tGameboy consoles. Used compiler: sdcc\n"
              << std::endl;
}


void printLicense()
{
    std::cerr << "Copyright 2021-2024 Moritz Strohm\n\n"
        "This program is free software: you can redistribute it and/or modify it under\n"
        "the terms of the GNU General Public License as published by the Free Software\n"
        "Foundation, either version 3 of the License, or (at your option) any later\n"
        "version.\n\n"

        "This program is distributed in the hope that it will be useful, but WITHOUT\n"
        "ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS\n"
        "FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n\n"

        "You should have received a copy of the GNU General Public License along with\n"
        "this program. If not, see <https://www.gnu.org/licenses/>.\n"
              << std::endl;
}


int main(int argc, char** argv)
{
    std::cerr << "ucmf - microcontroller make frontend\n";
    std::vector<std::string> args(argv, argv + argc);

    std::string directory = ".";

    if (args.size() > 1) {
        if (args[1] == "--help") {
            printHelp();
            return EXIT_SUCCESS;
        } else if (args[1] == "--help-uc-family") {
            printUcFamilyList();
            return EXIT_SUCCESS;
        } else if (args[1] == "--version") {
            std::cerr << fmt::format("Version {}\n", UCMF_VERSION);
            return EXIT_SUCCESS;
        } else if (args[1] == "--license") {
            printLicense();
            return EXIT_SUCCESS;
        }
        directory = args[1];
    }


    //Check for a project file in the specified directory:

    auto config = std::make_shared<UCMF::Config>();
    try {
        config->readConfigFile(directory + "/ucmf.config");
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    try {
        config->readConfigFile(directory + "/ucmf-local.config");
    } catch (std::exception& e) {
        //It is not a problem when the local config file cannot be read
        //since that file is optional.
    }

    auto uc_family = config->getEntry("UC_FAMILY");
    std::vector<std::string> sources;

    //Check if we have all relevant config keys:
    if (!config->hasEntry("PROJECT_NAME")) {
        std::cerr << "PROJECT_NAME missing in ucmf.config!" << std::endl;
        return EXIT_FAILURE;
    }
    if (uc_family.empty()) {
        std::cerr << "UC_FAMILY missing in ucmf.config!" << std::endl;
        return EXIT_FAILURE;
    }
    if (!config->hasEntry("UC_MODEL")) {
        std::cerr << "UC_MODEL missing in ucmf.config!" << std::endl;
        return EXIT_FAILURE;
    }
    if (!config->hasEntry("SOURCES") && !config->hasEntry(uc_family, "UC_FAMILY_SOURCES")) {
        std::cerr << "SOURCES missing in ucmf.config!" << std::endl;
        return EXIT_FAILURE;
    } else {
        //While we are at it, we can split the sources line:
        auto all_sources = config->getEntry("SOURCES")
            + " "
            + config->getEntry(uc_family, "UC_FAMILY_SOURCES");
        std::stringstream sources_stream(all_sources);
        sources.assign(
            std::istream_iterator<std::string>(sources_stream),
            std::istream_iterator<std::string>()
            );
    }

    std::shared_ptr<UCMF::Generator> generator = nullptr;

    if (uc_family.empty()) {
        std::cerr << "No microcontroller family specified! Cannot continue!" << std::endl;
        return EXIT_FAILURE;
    }

    if ((uc_family == "AVR" || uc_family == "ARDUINO")) {
        generator = std::make_shared<UCMF::AVRGenerator>();
    }/* else if (uc_family == "GAMEBOY") {
        generator = std::make_shared<UCMF::GameboyGenerator>();
    }*/

    if (generator == nullptr) {
        std::cerr << fmt::format(
            "No generator is available for the microcontroller family \"{}\"! Cannot continue!",
            uc_family
            ) << std::endl;
        return EXIT_FAILURE;
    }

    generator->setConfig(config);

    std::cerr << "Project configuration:\n\n"
              << "Name:\t\t" << config->getEntry("PROJECT_NAME") << "\n"
              << "Target family:\t" << config->getEntry("UC_FAMILY") << "\n"
              << "Target model:\t" << config->getEntry("UC_MODEL") << "\n";
    auto uc_package = config->getEntry("UC_PACKAGE");
    if (uc_package.empty()) {
        std::cerr << "\n";
    } else {
        std::cerr << "Target package:\t" << uc_package << "\n\n";
    }

    for (auto source: sources) {
        generator->addSourceFile(source);
    }

    //Generate the makefile:

    std::string makefile_content = "";
    try {
        makefile_content = generator->buildMakefile();
    } catch (std::invalid_argument& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    std::ofstream makefile(directory + "/Makefile", std::ofstream::out);
    if (!makefile.is_open()) {
        std::cerr << "Cannot produce Makefile!" << std::endl;
        return EXIT_FAILURE;
    }

    makefile << "default_target: build\n\n"
             << makefile_content << "\n\n";
    std::cerr << "ucmf has finished." << std::endl;
    return EXIT_SUCCESS;
}
