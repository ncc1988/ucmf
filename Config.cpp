/*
    This file is part of ucmf - microcontroller make frontend
    Copyright (C) 2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Config.h"


using namespace UCMF;


Config::Config()
{
    //Nothing at the moment.
}


void Config::readConfigFile(const std::string& config_file_path)
{
    std::ifstream project_file(config_file_path);
    if (!project_file.is_open()) {
        throw std::invalid_argument(
            fmt::format(
                "Cannot read config file {}!",
                config_file_path
                )
            );
    }

    std::string config_line = "";
    while (std::getline(project_file, config_line)) {
        //Split the config line by the first equal sign
        //to get key and value out of it.
        if (config_line.length() < 1) {
            //Nothing to do with emtpy config lines.
            continue;
        }
        if (config_line[0] == '#') {
            //A comment. Skip that line.
            continue;
        }
        size_t section_end_pos = config_line.find("::");
        size_t key_end_pos = config_line.find("=");
        //Config keys and values must not be empty.
        //Section markers ("::") must be omitted if an entry for the global
        //section shall be set.
        if ((section_end_pos > 0) && (section_end_pos != std::string::npos)) {
            if (section_end_pos + 2 < key_end_pos) {
                std::string section = config_line.substr(0, section_end_pos);
                //Place the key in the correct section.
                std::string key = config_line.substr(
                    section_end_pos + 2,
                    key_end_pos - (section_end_pos + 2)
                    );
                std::string value = config_line.substr(
                    key_end_pos + 1,
                    std::string::npos
                    );
                if (value.length() > 0) {
                    this->addEntry(key, value, section);
                }
            }
        } else {
            //Place the key in the global section.
            std::string key = config_line.substr(0, key_end_pos);
            std::string value = config_line.substr(
                key_end_pos + 1,
                std::string::npos
                );
            if (value.length() > 0) {
                this->addEntry(key, value);
            }
        }
    }
}


std::string Config::toConfigFile()
{
    std::string output;
    for (auto section_item: this->entries) {
        for (auto config_item: section_item.second) {
            if (section_item.first.length() > 0) {
                output += fmt::format(
                    "{}::{}={}\n",
                    section_item.first,
                    config_item.first,
                    config_item.second
                    );
            } else {
                output += fmt::format(
                    "{}={}\n",
                    config_item.first,
                    config_item.second
                    );
            }
        }
    }
    return output;
}


void Config::addEntry(
    const std::string& key,
    const std::string& value,
    const std::string& section
    )
{
    auto section_it = this->entries.find(section);
    if (section_it == this->entries.end()) {
        //Create a new section:
        std::map<std::string, std::string> new_section;
        new_section[key] = value;
        this->entries[section] = new_section;
    } else {
        //Add the entry to the section.
        section_it->second[key] = value;
    }
}

void Config::setEntry(
    const std::string& key,
    const std::string& value,
    const std::string& section
    )
{
    this->addEntry(key, value, section);
}


bool Config::hasEntry(
    const std::string& key,
    const std::string& section
    )
{
    auto section_it = this->entries.find(section);
    if (section_it == this->entries.end()) {
        return false;
    }
    auto pos = section_it->second.find(key);
    if (pos == section_it->second.end()) {
        return false;
    }
    return true;
}


std::string Config::getEntry(
    const std::string& key,
    const std::string& section
    )
{
    auto section_it = this->entries.find(section);
    if (section_it == this->entries.end()) {
        return "";
    }
    auto key_it = section_it->second.find(key);
    if (key_it == section_it->second.end()) {
        return "";
    }
    return key_it->second;
}


void Config::removeEntry(
    const std::string& key,
    const std::string& section
    )
{
    auto section_it = this->entries.find(section);
    if (section_it != this->entries.end()) {
        section_it->second.erase(key);
    }
}
